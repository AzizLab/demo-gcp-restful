import firebase_admin
from firebase_admin import credentials, firestore
from flask import Flask, jsonify, request
from flask_cors import CORS

app = Flask(__name__)

# |======== CORS CONFIG =========|
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'

cred = credentials.Certificate("./creds/tanitlabschool-dev-firebase-adminsdk-a8mmx-545c3c3282.json")
firebase_admin.initialize_app(cred)
dbf = firestore.client()


@app.route('/', methods=['GET'])
def hello():
    return "hello world"


@app.route('/customers', methods=['GET'])
def get_customers_list():
    customers = dbf.collection('customers').stream()
    customers = [c.to_dict() for c in customers]
    return jsonify(dict(data=customers)), 200


@app.route('/customers', methods=['POST'])
def create_new_customer():
    payload = request.get_json()
    dbf.collection('customers').add(payload)
    return jsonify(dict(message="customer added successfully")), 201


@app.route('/customers/<customer_id>', methods=['PUT'])
def edit_existing_customer(customer_id):
    payload = request.get_json()
    dbf.collection('customers').document(customer_id).set(payload, merge=True)
    return jsonify(dict(message="customer edited successfully")), 201


@app.route('/customers/<customer_id>', methods=['DELETE'])
def delete_customer(customer_id):
    dbf.collection('customers').document(customer_id).delete()
    return jsonify(dict(message="customer deleted successfully")), 201


if __name__ == '__main__':
    app.run(debug=True, port=5000)
